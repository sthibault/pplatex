---
date: 02/14/2024
section: 1
title: PPLATEX
---

# NAME

pplatex – parse logs of latex and print warnings and errors

# SYNOPSIS

**pplatex** \[*pplatex options*\] --] [*latex options*] *\<INPUT_TEX_OR_LOG\>*

# DESCRIPTION

LaTeX is able to produce really nice document layouts. But it is also able to
produce a lot of noise on the command line.  `pplatex` is a command-line tool
that parses the logs of latex and pdflatex and prints warnings and errors in an
human readable format.


# OPTIONS

**-c, --cmd \<cmd\>

> Execute \<cmd\> to compile the tex file

**-i, --input \<file\>**

> Parse logfile \<file\> instead of executing latex ('-' for stdin)

**-b**

> Do not show badbox messages

**-q**

> Do not show warnings and badbox messages

**-v**

> Be verbose

**-V, --version**

> Show version info

**-h, --help**

> Show this help

# NOTE

Debian NOTE: the following feature is NOT supported on Debian:

By default, if the program is called 'pplatex', 'latex' will be executed, if it
is called 'ppluatex' then 'lualatex' will be executed,
else 'pdflatex' will be used.

